﻿using AlexFirstApplication.ValueConverters;
using Xamarin.Forms;

namespace AlexFirstApplication
{
	public class ColleagueCell : ViewCell
	{
		public ColleagueCell ()
		{
			var image = new Image { HorizontalOptions = LayoutOptions.Start };
			image.SetBinding (Image.SourceProperty, "ImageSource");

			var nameLabel = new Label { HorizontalOptions = LayoutOptions.FillAndExpand };
			nameLabel.SetBinding (Label.TextProperty, "Name");

			var genderLabel = new Label { HorizontalOptions = LayoutOptions.End };
			genderLabel.SetBinding (Label.TextProperty, new Binding ("Gender", BindingMode.Default, new GenderToStringValueConverter (), null));

			var stack = new StackLayout { 
				Orientation = StackOrientation.Horizontal,
				Children = {
					image, nameLabel, genderLabel
				}
			};

			View = stack;
		}
	}
}
