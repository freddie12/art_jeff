﻿using AlexFirstApplication.Model.Colleague;
using Xamarin.Forms;

namespace AlexFirstApplication.Pages
{
	public class ColleaguePage : ContentPage
	{
		public ColleaguePage ()
		{
			Title = "Colleages";
			var colleagueList = new ListView ();
			colleagueList.ItemsSource = ColleagueManager.ColleagueList;
			colleagueList.ItemTemplate = new DataTemplate (typeof (ColleagueCell));

			Content = colleagueList;
		}
	}
}
