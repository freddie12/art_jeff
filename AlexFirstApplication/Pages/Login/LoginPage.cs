﻿using Pro;
using Xamarin.Forms;

namespace AlexFirstApplication.Pages
{
	public class LoginPage : ContentPage
	{
		ClearableEntry usernameEntry;
		ClearableEntry passwordEntry;
		Button loginButton;

		public LoginPage ()
		{
			Title = "Login";

			usernameEntry = new ClearableEntry { Placeholder = "Username" };

			passwordEntry = new ClearableEntry { Placeholder = "Password" };

			loginButton = new Button {
				Text = "Login",
				BackgroundColor = Color.Aqua,
				TextColor = Color.Gray
			};
			loginButton.Clicked += (sender, e) => { System.Diagnostics.Debug.WriteLine ("Login Button Clicked"); };

			var stack = new StackLayout {
				Margin = 20,
				Orientation = StackOrientation.Vertical,
				Children = {
					usernameEntry,
					passwordEntry,
					loginButton
				}
			};

			Content = stack;

		}
	}
}
