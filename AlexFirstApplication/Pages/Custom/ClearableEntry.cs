﻿using AlexFirstApplication;
using Xamarin.Forms;

namespace Pro
{
	public class ClearableEntry : Frame
	{
		BorderlessEntry entry;
		public BorderlessEntry Entry { get { return entry; } }

		Image image;

		public string Placeholder {
			get { return entry != null ? entry.Placeholder : "";  }
			set { if (entry != null) entry.Placeholder = value; }
		}

		public ClearableEntry ()
		{
			BuildLayout ();
		}

		void BuildLayout ()
		{
			SetupEntry ();
			SetupClearButton ();
			SetupGestures ();

			var stack = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Children = { entry, image }
			};

			OutlineColor = Color.Gray;
			Content = stack;
		}

		void SetupEntry ()
		{
			entry = new BorderlessEntry ();
			entry.HorizontalOptions = LayoutOptions.FillAndExpand;
			entry.Margin = new Thickness (0, 5, 0, 5);
			entry.HeightRequest = 24;
		}

		void SetupClearButton ()
		{
			image = new Image ();
			image.Source = "clear.png";
			image.HorizontalOptions = LayoutOptions.End;
			image.Margin = new Thickness (5);
		}

		void SetupGestures ()
		{ 
			var tapped = new TapGestureRecognizer ();
			tapped.Tapped += (sender, e) => {
				entry.Text = string.Empty;
			};
			image.GestureRecognizers.Add (tapped);

			entry.TextChanged += (sender, e) => {
				var i = string.IsNullOrEmpty (entry.Text);
				image.IsVisible = !i;
			};
		}
	}
}

