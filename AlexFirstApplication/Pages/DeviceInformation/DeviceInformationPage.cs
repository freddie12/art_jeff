﻿using AlexFirstApplication.DependancyService;
using Xamarin.Forms;

namespace AlexFirstApplication.Pages
{
	public class DeviceInformationPage : ContentPage
	{
		public DeviceInformationPage ()
		{
			Title = "Device Information";

			var deviceInformation = DependencyService.Get<IPlatformInfo> ();

			var deviceNameLabel = new Label ();
			deviceNameLabel.Text = deviceInformation.Name;

			var deviceVersionLabel = new Label ();
			deviceVersionLabel.Text = deviceInformation.Version;

			var deviceHardwareVersionLabel = new Label ();
			deviceHardwareVersionLabel.Text = deviceInformation.HardwareVersion;

			var stack = new StackLayout {
				Orientation = StackOrientation.Vertical,
				Children = {
					deviceNameLabel,
					deviceVersionLabel,
					deviceHardwareVersionLabel
				}
			};

			Content = stack;
		}
	}
}
