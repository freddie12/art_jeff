﻿using System;
using Xamarin.Forms;

namespace AlexFirstApplication.Pages
{
	public class DeviceInformationLandingPage : ContentPage
	{
		public DeviceInformationLandingPage ()
		{
			var button = new Button { Text = "Show Device Information" };
			button.Clicked += HandleButtonClicked;


			Content = button;
			Title = "Device Information";
		}

		async void HandleButtonClicked (object sender, EventArgs e)
		{
			await Navigation.PushAsync (new DeviceInformationPage ());
		}
	}
}
