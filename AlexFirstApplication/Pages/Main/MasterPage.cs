﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace AlexFirstApplication.Pages
{
	public class MasterPage : ContentPage
	{
		public MasterPage ()
		{
			var header = new Label () {
				Text = "Alex's Application"
			};

			var navItems = new List<MasterPageItem> {
				new MasterPageItem { Title = "Login", ImageSrc = null, TargetType = typeof (LoginPage) },
				new MasterPageItem { Title = "Device Information", ImageSrc = null, TargetType = typeof (DeviceInformationLandingPage) },
				new MasterPageItem { Title = "Colleagues", ImageSrc = null, TargetType = typeof (ColleaguePage) }
			};

			navList = new ListView {
				ItemsSource = navItems,
				ItemTemplate = new DataTemplate (() => {
					var imageCell = new ImageCell ();
					imageCell.SetBinding (TextCell.TextProperty, "Title");
					imageCell.SetBinding (ImageCell.ImageSourceProperty, "ImageSrc");
					return imageCell;
				})
			};

			Title = "Alex's App";
			Padding = new Thickness { Top = 5 };
			Content = new StackLayout {
				Children = { header, navList }
			};
		}

		ListView navList;
		public ListView NavList 
		{
			get { return navList; }
		}
	}
}
