﻿using System;
using Xamarin.Forms;

namespace AlexFirstApplication.Pages
{
	public class MainPage : MasterDetailPage
	{
		MasterPage masterPage;

		public MainPage () 
		{
			masterPage = new MasterPage ();
			Master = masterPage;
			Detail = new NavigationPage (new LoginPage ());

			masterPage.NavList.ItemSelected += HandleNavListItemSelected;
		}

		void HandleNavListItemSelected (object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as MasterPageItem;

			if (item == null)
				return;

			Detail = new NavigationPage ((Page)Activator.CreateInstance (item.TargetType));
			masterPage.NavList.SelectedItem = null;
			IsPresented = false;
		}
	}
}