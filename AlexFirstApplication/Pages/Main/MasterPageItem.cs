﻿using System;
using Xamarin.Forms;

namespace AlexFirstApplication.Pages
{
	public class MasterPageItem
	{
		public string Title { get; set; }
		public Image ImageSrc { get; set; }
		public Type TargetType { get; set; }
	}
}
