﻿using AlexFirstApplication;
using AlexFirstApplication.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer (typeof (NoHelpEntry), typeof (NoHelpEntryRenderer))]
namespace AlexFirstApplication.Droid
{
	public class NoHelpEntryRenderer : EntryRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged (e);

			if (Control == null)
				return;

			Control.InputType = Android.Text.InputTypes.TextFlagNoSuggestions;
		}

		public NoHelpEntryRenderer () {}
	}
}
