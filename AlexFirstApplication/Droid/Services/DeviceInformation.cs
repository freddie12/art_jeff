﻿using AlexFirstApplication.DependancyService;
using AlexFirstApplication.Droid;
using Xamarin.Forms;
using Plugin.DeviceInfo;

[assembly: Dependency (typeof (DeviceInformation))]
namespace AlexFirstApplication.Droid
{
	public class DeviceInformation : IPlatformInfo
	{
		public string HardwareVersion { get { return GetHardwareVersion (); } }
		public string Name { get { return GetDeviceName (); } }
		public string Version { get { return GetVersion (); } }


		string GetHardwareVersion ()
		{
			return CrossDeviceInfo.Current.Model;
		}

		string GetVersion ()
		{
			return CrossDeviceInfo.Current.Version;
		}

		string GetDeviceName ()
		{
			return CrossDeviceInfo.Current.Id;
		}
	}
}
