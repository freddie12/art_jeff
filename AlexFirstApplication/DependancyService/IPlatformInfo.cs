﻿namespace AlexFirstApplication.DependancyService
{
	public interface IPlatformInfo
	{
		string Name { get; }
		string Version { get; }
		string HardwareVersion { get; }
	}
}
