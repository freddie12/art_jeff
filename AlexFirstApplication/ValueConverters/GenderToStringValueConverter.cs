﻿using System;
using System.Globalization;
using AlexFirstApplication.Model.Colleague;
using Xamarin.Forms;

namespace AlexFirstApplication.ValueConverters
{
	public class GenderToStringValueConverter : IValueConverter
	{
		public object Convert (object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value != null && value is Gender) 
			{
				return (Gender) value == Gender.M ? "Male" : "Female";
			}
			return value.ToString ();
		}

		public object ConvertBack (object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException ();
		}
	}
}
