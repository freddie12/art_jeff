﻿using System;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;
using Pro;
using Pro.iOS;
using UIKit;
using CoreGraphics;
using Foundation;
using AlexFirstApplication;

[assembly: ExportRenderer (typeof (BorderlessEntry), typeof (BorderlessEntryiOS))]
namespace Pro.iOS
{
	public class BorderlessEntryiOS : EntryRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Xamarin.Forms.Entry> e)
		{
			base.OnElementChanged (e);

			if (Control != null) {
				Control.BorderStyle = UITextBorderStyle.None;
			}
		}
	}
}

