﻿
using AlexFirstApplication;
using AlexFirstApplication.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer (typeof (NoHelpEntry), typeof (NoHelpEntryRenderer))]
namespace AlexFirstApplication.iOS
{
	public class NoHelpEntryRenderer : EntryRenderer
	{
		protected override void OnElementChanged (ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged (e);

			if (Control == null)
				return;

			Control.SpellCheckingType = UIKit.UITextSpellCheckingType.No;
			Control.AutocorrectionType = UIKit.UITextAutocorrectionType.No;
		}

		public NoHelpEntryRenderer ()
		{
		}
	}
}
