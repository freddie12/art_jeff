﻿using System.Collections.ObjectModel;

namespace AlexFirstApplication.Model.Colleague
{
	public class ColleagueManager
	{
		public static ObservableCollection <Colleague> ColleagueList {
			get;
		} = new ObservableCollection <Colleague> {
			new Colleague {Name = "Jeff", Gender=Gender.M, ImageSource = "donald.jpg"},
			new Colleague {Name = "Rachel", Gender=Gender.F, ImageSource = "fairy.jpg"},
			new Colleague {Name = "Lee", Gender=Gender.M, ImageSource = "jerry.jpg"},
   			new Colleague {Name = "Steve", Gender=Gender.M, ImageSource = "mickey.jpg"},
			new Colleague {Name = "Anu", Gender=Gender.F, ImageSource = "leela.jpg"},
			new Colleague {Name = "Andrew", Gender=Gender.M, ImageSource = "guy.jpg"},
			new Colleague {Name = "Andrien", Gender=Gender.M, ImageSource = "popeye.jpg"},
			new Colleague {Name = "Richard.S", Gender=Gender.M, ImageSource = "scoobie.png"},
			new Colleague {Name = "Richard.C", Gender=Gender.M, ImageSource = "stewie.jpg"},
			new Colleague {Name = "Warren", Gender=Gender.M, ImageSource = "taz.jpg"},
			new Colleague {Name = "Chris", Gender=Gender.M, ImageSource = "scoobie.png"},
			new Colleague {Name = "Gary", Gender=Gender.M, ImageSource = "mickey.jpg"},
			new Colleague {Name = "Adri", Gender=Gender.F, ImageSource = "cinderella.jpg"},
			new Colleague {Name = "Paul", Gender=Gender.M, ImageSource = "popeye.jpg"},
			new Colleague {Name = "Tim", Gender=Gender.M, ImageSource = "stewie.jpg"}
		};
	}

	public struct Colleague
	{ 
		public string Name { get; internal set; }
		public Gender Gender { get; internal set; }
		public string ImageSource { get; internal set; }
	}

	public enum Gender
	{
		M,
		F
	}
}
